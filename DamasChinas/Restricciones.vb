﻿Public Class Restricciones
    Dim circulos(120) As Ellipse
    Dim fichaSelecionar As Object
    Dim ellipseSeleccionar As Ellipse
    Private Sub saltar(sender As Object)
        For contador As Integer = 0 To 120
            Dim X As Integer = 5
            Dim Y As Integer = 5
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 40) Then
                X = 40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 20) Then
                X = 20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 20) Then
                X = -20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 40) Then
                X = -40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender)) Then
                X = 0
            End If

            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) + 40) Then
                Y = 40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) - 40) Then
                Y = -40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender)) Then
                Y = 0
            End If
            If Not X = 5 And Not Y = 5 Then
                If Not circulos(contador).Fill.Equals(Brushes.Red) Then
                Else
                    For a As Integer = 0 To 120
                        If Canvas.GetLeft(circulos(a)) = (Canvas.GetLeft(sender) + X * 2) And Canvas.GetTop(circulos(a)) = (Canvas.GetTop(sender) + Y * 2) Then
                            If Not circulos(a).Fill.Equals(Brushes.Red) And Not circulos(a).Fill.Equals(Brushes.Pink) Then
                                circulos(a).Fill = Brushes.Pink
                                saltar(circulos(a))
                            End If
                        End If
                    Next
                End If
            End If

        Next
    End Sub
    Private Sub TableroMouseLeftButtonDown(sender As Object, e As MouseEventArgs)
        fichaSelecionar = sender
        For contador As Integer = 0 To 120
            Dim X As Integer = 5
            Dim Y As Integer = 5
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 40) Then
                X = 40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 20) Then
                X = 20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 20) Then
                X = -20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 40) Then
                X = -40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender)) Then
                X = 0
            End If

            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) + 40) Then
                Y = 40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) - 40) Then
                Y = -40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender)) Then
                Y = 0
            End If
            If Not X = 5 And Not Y = 5 Then
                If Not circulos(contador).Fill.Equals(Brushes.Red) Then
                    circulos(contador).Fill = Brushes.Pink
                Else
                    For a As Integer = 0 To 120
                        If Canvas.GetLeft(circulos(a)) = (Canvas.GetLeft(fichaSelecionar) + X * 2) And Canvas.GetTop(circulos(a)) = (Canvas.GetTop(fichaSelecionar) + Y * 2) Then
                            If Not circulos(a).Fill.Equals(Brushes.Red) Then
                                circulos(a).Fill = Brushes.Pink
                                saltar(circulos(a))
                                ' saltar((Canvas.GetLeft(fichaSelecionar) + X * 2), (Canvas.GetTop(fichaSelecionar) + Y * 2))
                            End If
                        End If
                    Next
                End If
            End If

        Next
    End Sub
    'con este metodo ponemos las restriccion de turno, movimiento no valido, y  ha selecionar el ellipse de posicion de 6 jugadores
    Private Sub TablerosMouseLeftButtonDown(sender As Object, e As MouseEventArgs)
        ellipseSeleccionar = sender

        If Not (IsNothing(fichaSelecionar)) Then


            If sender.fill.Equals(Brushes.Pink) Then
                Canvas.SetLeft(fichaSelecionar, CDbl(Canvas.GetLeft(sender)))
                Canvas.SetTop(fichaSelecionar, CDbl(Canvas.GetTop(sender)))
                fichaSelecionar = Nothing
            Else
                MsgBox("movimiento no valido", MsgBoxStyle.Critical)
            End If

        Else
            MsgBox("seleccione una ficha", MsgBoxStyle.Information)
        End If
    End Sub
    'con este metodo selecionamos el control de usario y le enseñamos los movimientos validos para 3 jugadore
    Private Sub Tablero3MouseLeftButtonDown(sender As Object, e As MouseEventArgs)
        fichaSelecionar = sender
        For contador As Integer = 0 To 120
            Dim X As Integer = 5
            Dim Y As Integer = 5
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 40) Then
                X = 40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 20) Then
                X = 20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 20) Then
                X = -20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 40) Then
                X = -40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender)) Then
                X = 0
            End If

            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) + 40) Then
                Y = 40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) - 40) Then
                Y = -40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender)) Then
                Y = 0
            End If
            If Not X = 5 And Not Y = 5 Then
                If Not circulos(contador).Fill.Equals(Brushes.Red) Then
                    circulos(contador).Fill = Brushes.Pink
                Else
                    For a As Integer = 0 To 120
                        If Canvas.GetLeft(circulos(a)) = (Canvas.GetLeft(fichaSelecionar) + X * 2) And Canvas.GetTop(circulos(a)) = (Canvas.GetTop(fichaSelecionar) + Y * 2) Then
                            If Not circulos(a).Fill.Equals(Brushes.Red) Then
                                circulos(a).Fill = Brushes.Pink
                                saltar(circulos(a))
                            End If
                        End If
                    Next
                End If
            End If
        Next

    End Sub
    'con este metodo ponemos las restriccion de turno, movimiento no valido, y  ha selecionar el ellipse de posicion de 3 jugadores
    Private Sub Tableros3MouseLeftButtonDown(sender As Object, e As MouseEventArgs)
        If Not (IsNothing(fichaSelecionar)) Then
            If sender.fill.Equals(Brushes.Pink) Then

                Canvas.SetLeft(fichaSelecionar, CDbl(Canvas.GetLeft(sender)))
                Canvas.SetTop(fichaSelecionar, CDbl(Canvas.GetTop(sender)))

                fichaSelecionar = Nothing
               
            Else
                MsgBox("movimiento no valido", MsgBoxStyle.Critical)

            End If
        Else
            MsgBox("No es su Turno", MsgBoxStyle.Critical)
        End If
       
    End Sub
End Class
