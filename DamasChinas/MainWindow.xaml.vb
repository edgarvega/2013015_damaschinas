﻿Class MainWindow
    Dim circulos(120) As Ellipse
    Dim contadorForTablero As Integer = 0
    Dim fichas(59) As UserControl
    Dim fichas3(44) As UserControl
    Dim fichaSelecionar As Object
    Dim jugadorAzul(9) As Ellipse
    Dim ellipseSeleccionar As Ellipse
    Dim ellepseNegro(9) As Ellipse
    Dim ellepseRojo(9) As Ellipse
    Dim ellepseMorado(9) As Ellipse
    Dim ellepseNaranaja(9) As Ellipse
    Dim ellepseVerificarVerde(14) As Ellipse
    Dim ellepseVerificarNaranja(14) As Ellipse
    Dim ellepseVerificarMorado(14) As Ellipse
    Dim turno As Integer = 1
    Dim turno3 As Integer = 1
    Dim turnoValidar As Boolean
    Dim click6 As Boolean
    Dim click3 As Boolean
    Dim Contadorturnos As Integer = 1
    Dim Contadorturnos3jugadores As Integer = 1
    'este metodo es para crear el tablero de elllipses
    Private Sub TableroEllipse()
        Dim left As Integer = 0
        Dim top As Integer = 0
        For y As Integer = 0 To 16
            For x As Integer = 0 To 12
                Dim tamañoCirculos As New Ellipse
                tamañoCirculos.Width = 40
                tamañoCirculos.Height = 40
                tamañoCirculos.Stroke = Brushes.Black
                tamañoCirculos.Fill = Brushes.Cornsilk
                If y Mod 2 = 0 Then
                    Canvas.SetLeft(tamañoCirculos, CDbl(left + 140))
                Else
                    Canvas.SetLeft(tamañoCirculos, CDbl(left + 160))
                End If
                Canvas.SetTop(tamañoCirculos, CDbl(top))
                Select Case y
                    Case 0, 16
                        If (x = 6) Then
                            tablero.Children.Add(tamañoCirculos)
                            circulos(contadorForTablero) = tamañoCirculos
                            contadorForTablero += 1
                        End If
                    Case 1, 15
                        If (x = 6 Or x = 5) Then
                            tablero.Children.Add(tamañoCirculos)
                            circulos(contadorForTablero) = tamañoCirculos
                            contadorForTablero += 1
                        End If
                    Case 2, 14
                        If (x = 5 Or x = 6 Or x = 7) Then
                            tablero.Children.Add(tamañoCirculos)
                            circulos(contadorForTablero) = tamañoCirculos
                            contadorForTablero += 1
                        End If
                    Case 3, 13
                        If (x = 4 Or x = 5 Or x = 6 Or x = 7) Then
                            tablero.Children.Add(tamañoCirculos)
                            circulos(contadorForTablero) = tamañoCirculos
                            contadorForTablero += 1
                        End If
                    Case 4, 12
                        tablero.Children.Add(tamañoCirculos)
                        circulos(contadorForTablero) = tamañoCirculos
                        contadorForTablero += 1
                    Case 5, 11
                        If (x = 0 Or x = 1 Or x = 2 Or x = 3 Or x = 4 Or x = 5 Or x = 6 Or x = 7 Or x = 8 Or x = 9 Or x = 10 Or x = 11) Then
                            tablero.Children.Add(tamañoCirculos)
                            circulos(contadorForTablero) = tamañoCirculos
                            contadorForTablero += 1
                        End If
                    Case 6, 10
                        If (x = 1 Or x = 2 Or x = 3 Or x = 4 Or x = 5 Or x = 6 Or x = 7 Or x = 8 Or x = 9 Or x = 10 Or x = 11) Then
                            tablero.Children.Add(tamañoCirculos)
                            circulos(contadorForTablero) = tamañoCirculos
                            contadorForTablero += 1
                        End If
                    Case 7, 9
                        If (x = 1 Or x = 2 Or x = 3 Or x = 4 Or x = 5 Or x = 6 Or x = 7 Or x = 8 Or x = 9 Or x = 10) Then
                            tablero.Children.Add(tamañoCirculos)
                            circulos(contadorForTablero) = tamañoCirculos
                            contadorForTablero += 1
                        End If
                    Case 8, 10
                        If (x = 2 Or x = 3 Or x = 4 Or x = 5 Or x = 6 Or x = 7 Or x = 8 Or x = 9 Or x = 10) Then
                            tablero.Children.Add(tamañoCirculos)
                            circulos(contadorForTablero) = tamañoCirculos
                            contadorForTablero += 1
                        End If
                End Select
                left += 40
            Next
            top += 40
            left = 0
        Next
    End Sub
    'este metodo agrega los controles de usuario para 6 jugadores
    Private Sub controlUsuario6Jugadores()
        Dim Left As Double 'left y top son las cordenadas donde seran colocadas las fichas
        Dim Top As Double
        Dim contador As Integer ' este contador para agregar las fichas azules a la matriz 
        Dim contador1 As Integer ' este contador es para agregar a las demas fichas a la matriz
        Dim contadornegras As Integer
        Dim contadorMoradas As Integer
        Dim contadorRojo As Integer
        Dim contadorNaranjas As Integer
        Dim contadorazul As Integer = 0
        contador = 10
        contador1 = 20

        '--------------- VERDE ---------------------------------------------------------
        'colocamos la fichas verdes en su posiciones igual los demas for con su respectivos colores
        For a As Integer = 0 To 9
            circulos(a).Fill = Brushes.Green
            Dim verde As New fichaVerde
            Left = Canvas.GetLeft(circulos(a))
            Top = Canvas.GetTop(circulos(a))
            Canvas.SetLeft(verde, Left)
            Canvas.SetTop(verde, Top)
            tablero.Children.Add(verde)
            fichas(a) = verde
            '--------------- AZUL ---------------------------------------------------------
        Next
        For a As Integer = 111 To 120
            circulos(a).Fill = Brushes.Blue
            Dim azul As New fichaAzul
            fichas(contador) = azul
            contador = contador + 1
            Left = Canvas.GetLeft(circulos(a))
            Top = Canvas.GetTop(circulos(a))
            Canvas.SetLeft(azul, Left)
            Canvas.SetTop(azul, Top)
            tablero.Children.Add(azul)
            jugadorAzul(contadorazul) = circulos(a)
            contadorazul = contadorazul + 1
        Next
        '--------------- negro ---------------------------------------------------------
        For a As Integer = 10 To 13
            circulos(a).Fill = Brushes.Black
            Dim amarrilo As New fichaAmarrila
            fichas(contador1) = amarrilo
            contador1 = contador1 + 1
            Left = Canvas.GetLeft(circulos(a))
            Top = Canvas.GetTop(circulos(a))
            Canvas.SetLeft(amarrilo, Left)
            Canvas.SetTop(amarrilo, Top)
            tablero.Children.Add(amarrilo)
            ellepseNegro(contadornegras) = circulos(a)
            contadornegras = contadornegras + 1

        Next
        contador1 = 24
        contadornegras = 4
        For b As Integer = 23 To 25
            circulos(b).Fill = Brushes.Black
            Left = Canvas.GetLeft(circulos(b))
            Top = Canvas.GetTop(circulos(b))
            Dim amarrilo1 As New fichaAmarrila
            fichas(contador1) = amarrilo1
            contador1 = contador1 + 1
            Canvas.SetLeft(amarrilo1, Left)
            Canvas.SetTop(amarrilo1, Top)
            tablero.Children.Add(amarrilo1)
            ellepseNegro(contadornegras) = circulos(b)
            contadornegras = contadornegras + 1
        Next
        contador1 = 27
        contador = 7
        For c As Integer = 35 To 36
            circulos(c).Fill = Brushes.Black
            circulos(46).Fill = Brushes.Black
            Left = Canvas.GetLeft(circulos(c))
            Top = Canvas.GetTop(circulos(c))
            Dim amarrilo2 As New fichaAmarrila
            fichas(contador1) = amarrilo2
            contador1 = contador1 + 1
            Canvas.SetLeft(amarrilo2, Left)
            Canvas.SetTop(amarrilo2, Top)
            tablero.Children.Add(amarrilo2)
            ellepseNegro(contadornegras) = circulos(c)
            contadornegras = contadornegras + 1

        Next
        contadornegras = 10
        Left = Canvas.GetLeft(circulos(46))
        Top = Canvas.GetTop(circulos(46))
        Dim amarrilo3 As New fichaAmarrila
        Canvas.SetLeft(amarrilo3, Left)
        Canvas.SetTop(amarrilo3, Top)
        tablero.Children.Add(amarrilo3)
        fichas(29) = amarrilo3
        ellepseNegro(9) = circulos(46)
        '--------------- ROJO ---------------------------------------------------------
        contador1 = 30
        For a As Integer = 19 To 22
            circulos(a).Fill = Brushes.Red
            Dim roja As New fichaRoja
            fichas(contador1) = roja
            contador1 = contador1 + 1
            Left = Canvas.GetLeft(circulos(a))
            Top = Canvas.GetTop(circulos(a))
            Canvas.SetLeft(roja, Left)
            Canvas.SetTop(roja, Top)
            tablero.Children.Add(roja)
            ellepseRojo(contadorRojo) = circulos(a)
            contadorRojo = contadorRojo + 1
        Next
        contador1 = 34
        contadorRojo = 4
        For b As Integer = 32 To 34
            circulos(b).Fill = Brushes.Red
            Left = Canvas.GetLeft(circulos(b))
            Top = Canvas.GetTop(circulos(b))
            Dim roja1 As New fichaRoja
            fichas(contador1) = roja1
            contador1 = contador1 + 1
            Canvas.SetLeft(roja1, Left)
            Canvas.SetTop(roja1, Top)
            tablero.Children.Add(roja1)
            ellepseRojo(contadorRojo) = circulos(b)
            contadorRojo = contadorRojo + 1
        Next
        contador1 = 37
        contadorRojo = 7
        For c As Integer = 44 To 45
            circulos(c).Fill = Brushes.Red
            circulos(55).Fill = Brushes.Red
            Left = Canvas.GetLeft(circulos(c))
            Top = Canvas.GetTop(circulos(c))
            Dim roja2 As New fichaRoja
            fichas(contador1) = roja2
            contador1 = contador1 + 1
            Canvas.SetLeft(roja2, Left)
            Canvas.SetTop(roja2, Top)
            tablero.Children.Add(roja2)
            ellepseRojo(contadorRojo) = circulos(c)
            contadorRojo = contadorRojo + 1
        Next

        Left = Canvas.GetLeft(circulos(55))
        Top = Canvas.GetTop(circulos(55))
        Dim roja3 As New fichaRoja
        Canvas.SetLeft(roja3, Left)
        Canvas.SetTop(roja3, Top)
        tablero.Children.Add(roja3)
        fichas(39) = roja3
        ellepseRojo(9) = circulos(55)

        '--------------- ANARANJADO ---------------------------------------------------------
        contador1 = 40
        For a As Integer = 98 To 101
            circulos(a).Fill = Brushes.Orange
            Dim orange As New fichaOrange
            fichas(contador1) = orange
            contador1 = contador1 + 1
            Left = Canvas.GetLeft(circulos(a))
            Top = Canvas.GetTop(circulos(a))
            Canvas.SetLeft(orange, Left)
            Canvas.SetTop(orange, Top)
            tablero.Children.Add(orange)
            ellepseNaranaja(contadorNaranjas) = circulos(a)
            contadorNaranjas = contadorNaranjas + 1
        Next
        contador1 = 44
        contadorNaranjas = 4
        For b As Integer = 86 To 88
            circulos(b).Fill = Brushes.Orange
            Left = Canvas.GetLeft(circulos(b))
            Top = Canvas.GetTop(circulos(b))
            Dim orange1 As New fichaOrange
            fichas(contador1) = orange1
            contador1 = contador1 + 1
            Canvas.SetLeft(orange1, Left)
            Canvas.SetTop(orange1, Top)
            tablero.Children.Add(orange1)
            ellepseNaranaja(contadorNaranjas) = circulos(b)
            contadorNaranjas = contadorNaranjas + 1

        Next
        contador1 = 47
        contadorNaranjas = 7
        For c As Integer = 75 To 76
            circulos(c).Fill = Brushes.Orange
            circulos(65).Fill = Brushes.Orange
            Left = Canvas.GetLeft(circulos(c))
            Top = Canvas.GetTop(circulos(c))
            Dim orange2 As New fichaOrange
            fichas(contador1) = orange2
            contador1 = contador1 + 1
            Canvas.SetLeft(orange2, Left)
            Canvas.SetTop(orange2, Top)
            tablero.Children.Add(orange2)
            ellepseNaranaja(contadorNaranjas) = circulos(c)
            contadorNaranjas = contadorNaranjas + 1
        Next
        Left = Canvas.GetLeft(circulos(65))
        Top = Canvas.GetTop(circulos(65))
        Dim orange3 As New fichaOrange
        Canvas.SetLeft(orange3, Left)
        Canvas.SetTop(orange3, Top)
        tablero.Children.Add(orange3)
        fichas(49) = orange3
        contador1 = 50
        ellepseNaranaja(9) = circulos(65)
        '--------------- morado ---------------------------------------------------------

        For c As Integer = 107 To 110
            circulos(c).Fill = Brushes.Purple
            circulos(74).Fill = Brushes.Purple
            Left = Canvas.GetLeft(circulos(c))
            Top = Canvas.GetTop(circulos(c))
            Dim orange2 As New fichaGris
            fichas(contador1) = orange2
            contador1 = contador1 + 1
            Canvas.SetLeft(orange2, Left)
            Canvas.SetTop(orange2, Top)
            tablero.Children.Add(orange2)
            ellepseMorado(contadorMoradas) = circulos(c)
            contadorMoradas = contadorMoradas + 1
        Next
        contador1 = 54
        contadorMoradas = 4
        For b As Integer = 95 To 97
            circulos(b).Fill = Brushes.Purple
            Left = Canvas.GetLeft(circulos(b))
            Top = Canvas.GetTop(circulos(b))
            Dim gris1 As New fichaGris
            fichas(contador1) = gris1
            contador1 = contador1 + 1
            Canvas.SetLeft(gris1, Left)
            Canvas.SetTop(gris1, Top)
            tablero.Children.Add(gris1)
            ellepseMorado(contadorMoradas) = circulos(b)
            contadorMoradas = contadorMoradas + 1
        Next
        contador1 = 57
        contadorMoradas = 7
        For a As Integer = 84 To 85
            circulos(a).Fill = Brushes.Purple
            Dim gris As New fichaGris
            fichas(contador1) = gris
            contador1 = contador1 + 1
            Left = Canvas.GetLeft(circulos(a))
            Top = Canvas.GetTop(circulos(a))
            Canvas.SetLeft(gris, Left)
            Canvas.SetTop(gris, Top)
            tablero.Children.Add(gris)
            ellepseMorado(contadorMoradas) = circulos(a)
            contadorMoradas = contadorMoradas + 1
        Next

        Left = Canvas.GetLeft(circulos(74))
        Top = Canvas.GetTop(circulos(74))
        Dim gris3 As New fichaGris
        Canvas.SetLeft(gris3, Left)
        Canvas.SetTop(gris3, Top)
        tablero.Children.Add(gris3)
        fichas(59) = gris3
        ellepseMorado(9) = circulos(74)

        '------------------------------------------------------------------------------
        ' agregas el evento a los control de usario
        For x As Integer = 0 To 59
            AddHandler fichas(x).MouseLeftButtonDown, AddressOf TableroMouseLeftButtonDown
        Next
        'agregamos el evento a los ellipses
        For x As Integer = 0 To circulos.Length - 1
            AddHandler circulos(x).MouseLeftButtonDown, AddressOf TablerosMouseLeftButtonDown
        Next

    End Sub
    'este metodo agrega los controles de usuario para 3 jugadores
    Private Sub controlUsuario3Jugadores()
        'este metodo hace lo mismo que el controldeUsuario de 6 jugadores solo que este esta
        'echo para 3 jugadores.
        Dim Left As Double
        Dim Top As Double
        Dim contador1 As Integer = 0
        Dim contadorVerificarV As Integer
        Dim contadorVerificarN As Integer
        Dim contadorVerificarM As Integer

        For a As Integer = 0 To 9
            Dim verde As New fichaVerde
            Left = Canvas.GetLeft(circulos(a))
            Top = Canvas.GetTop(circulos(a))
            Canvas.SetLeft(verde, Left)
            Canvas.SetTop(verde, Top)
            tablero.Children.Add(verde)
            fichas3(a) = verde
            contador1 = contador1 + 1
        Next

        For a As Integer = 14 To 18
            Dim verde As New fichaVerde
            Left = Canvas.GetLeft(circulos(a))
            Top = Canvas.GetTop(circulos(a))
            Canvas.SetLeft(verde, Left)
            Canvas.SetTop(verde, Top)
            tablero.Children.Add(verde)
            fichas3(contador1) = verde
            contador1 = contador1 + 1
     Next

        '----------- ANARANJADO ----------------------------------------
        For a As Integer = 98 To 102
            Dim orange As New fichaOrange
            fichas3(contador1) = orange
            contador1 = contador1 + 1
            Left = Canvas.GetLeft(circulos(a))
            Top = Canvas.GetTop(circulos(a))
            Canvas.SetLeft(orange, Left)
            Canvas.SetTop(orange, Top)
            tablero.Children.Add(orange)
            Next

        For b As Integer = 86 To 89
            Left = Canvas.GetLeft(circulos(b))
            Top = Canvas.GetTop(circulos(b))
            Dim orange1 As New fichaOrange
            fichas3(contador1) = orange1
            contador1 = contador1 + 1
            Canvas.SetLeft(orange1, Left)
            Canvas.SetTop(orange1, Top)
            tablero.Children.Add(orange1)
        Next

        For c As Integer = 75 To 77
            Left = Canvas.GetLeft(circulos(c))
            Top = Canvas.GetTop(circulos(c))
            Dim orange2 As New fichaOrange
            fichas3(contador1) = orange2
            contador1 = contador1 + 1
            Canvas.SetLeft(orange2, Left)
            Canvas.SetTop(orange2, Top)
            tablero.Children.Add(orange2)
        Next

        Left = Canvas.GetLeft(circulos(65))
        Top = Canvas.GetTop(circulos(65))
        Dim orange3 As New fichaOrange
        Canvas.SetLeft(orange3, Left)
        Canvas.SetTop(orange3, Top)
        tablero.Children.Add(orange3)
        fichas3(contador1) = orange3
        contador1 = contador1 + 1
       
        Left = Canvas.GetLeft(circulos(66))
        Top = Canvas.GetTop(circulos(66))
        Dim orange4 As New fichaOrange
        Canvas.SetLeft(orange4, Left)
        Canvas.SetTop(orange4, Top)
        tablero.Children.Add(orange4)
        fichas3(contador1) = orange4
        contador1 = contador1 + 1

        Left = Canvas.GetLeft(circulos(56))
        Top = Canvas.GetTop(circulos(56))
        Dim orange5 As New fichaOrange
        Canvas.SetLeft(orange5, Left)
        Canvas.SetTop(orange5, Top)
        tablero.Children.Add(orange5)
        fichas3(contador1) = orange5
        contador1 = contador1 + 1

        '--------------- morado ---------------------------------------------------------
        For c As Integer = 106 To 110
            Left = Canvas.GetLeft(circulos(c))
            Top = Canvas.GetTop(circulos(c))
            Dim orange2 As New fichaGris
            fichas3(contador1) = orange2
            contador1 = contador1 + 1
            Canvas.SetLeft(orange2, Left)
            Canvas.SetTop(orange2, Top)
            tablero.Children.Add(orange2)
        Next

        For b As Integer = 94 To 97
            Left = Canvas.GetLeft(circulos(b))
            Top = Canvas.GetTop(circulos(b))
            Dim gris1 As New fichaGris
            fichas3(contador1) = gris1
            contador1 = contador1 + 1
            Canvas.SetLeft(gris1, Left)
            Canvas.SetTop(gris1, Top)
            tablero.Children.Add(gris1)
        Next

        For a As Integer = 83 To 85
            Dim gris As New fichaGris
            fichas3(contador1) = gris
            contador1 = contador1 + 1
            Left = Canvas.GetLeft(circulos(a))
            Top = Canvas.GetTop(circulos(a))
            Canvas.SetLeft(gris, Left)
            Canvas.SetTop(gris, Top)
            tablero.Children.Add(gris)
        Next

        Left = Canvas.GetLeft(circulos(74))
        Top = Canvas.GetTop(circulos(74))
        Dim gris3 As New fichaGris
        Canvas.SetLeft(gris3, Left)
        Canvas.SetTop(gris3, Top)
        tablero.Children.Add(gris3)
        fichas3(contador1) = gris3
        contador1 = contador1 + 1

        Left = Canvas.GetLeft(circulos(73))
        Top = Canvas.GetTop(circulos(73))
        Dim gris4 As New fichaGris
        Canvas.SetLeft(gris4, Left)
        Canvas.SetTop(gris4, Top)
        tablero.Children.Add(gris4)
        fichas3(contador1) = gris4
        contador1 = contador1 + 1
       
        Left = Canvas.GetLeft(circulos(64))
        Top = Canvas.GetTop(circulos(64))
        Dim gris5 As New fichaGris
        Canvas.SetLeft(gris5, Left)
        Canvas.SetTop(gris5, Top)
        tablero.Children.Add(gris5)
        fichas3(contador1) = gris5
        '----------------------------------------------------------------
        For a As Integer = 10 To 14
            ellepseVerificarMorado(contadorVerificarM) = circulos(a)
            contadorVerificarM = contadorVerificarM + 1
        Next
       
        For b As Integer = 23 To 26
            ellepseVerificarMorado(contadorVerificarM) = circulos(b)
            contadorVerificarM = contadorVerificarM + 1
        Next
       
        For c As Integer = 35 To 37
            ellepseVerificarMorado(contadorVerificarM) = circulos(c)
            contadorVerificarM = contadorVerificarM + 1
        Next
        ellepseVerificarMorado(contadorVerificarM) = circulos(46)
        contadorVerificarM = contadorVerificarM + 1
        ellepseVerificarMorado(contadorVerificarM) = circulos(47)
        contadorVerificarM = contadorVerificarM + 1
        ellepseVerificarMorado(contadorVerificarM) = circulos(56)


        '--------------- ROJO ---------------------------------------------------------
        For a As Integer = 18 To 22
            ellepseVerificarNaranja(contadorVerificarN) = circulos(a)
            contadorVerificarN = contadorVerificarN + 1
        Next
       
        For b As Integer = 31 To 34
            ellepseVerificarNaranja(contadorVerificarN) = circulos(b)
            contadorVerificarN = contadorVerificarN + 1
        Next
       
        For c As Integer = 43 To 45
            ellepseVerificarNaranja(contadorVerificarN) = circulos(c)
            contadorVerificarN = contadorVerificarN + 1
        Next
        ellepseVerificarNaranja(contadorVerificarN) = circulos(55)
        contadorVerificarN = contadorVerificarN + 1
        ellepseVerificarNaranja(contadorVerificarN) = circulos(54)
        contadorVerificarN = contadorVerificarN + 1
        ellepseVerificarNaranja(contadorVerificarN) = circulos(64)


        '--------------- azul ---------------------------------------------------------
        For a As Integer = 111 To 120
            ellepseVerificarVerde(contadorVerificarV) = circulos(a)
            contadorVerificarV = contadorVerificarV + 1

        Next

        For a As Integer = 102 To 106
            ellepseVerificarVerde(contadorVerificarV) = circulos(a)
            contadorVerificarV = contadorVerificarV + 1

        Next
       
        '------------------------------------------------------------------------------
        For x As Integer = 0 To 44
            AddHandler fichas3(x).MouseLeftButtonDown, AddressOf Tablero3MouseLeftButtonDown
        Next
        For x As Integer = 0 To circulos.Length - 1
            AddHandler circulos(x).MouseLeftButtonDown, AddressOf Tableros3MouseLeftButtonDown
        Next

    End Sub

    'METODOS PARA 3 JUGADORE 
    ' este metodo ayuda a borrar el color de los movimientos permitidos para 3 jugadore
    Private Sub regresarcolores3()
        For a As Integer = 0 To 120
            circulos(a).Fill = Brushes.Cornsilk
        Next
        For a As Integer = 0 To 9
            circulos(a).Fill = Brushes.LightGreen
        Next

        For a As Integer = 14 To 18
            circulos(a).Fill = Brushes.LightGreen
        Next

        For a As Integer = 98 To 102
            circulos(a).Fill = Brushes.Orange
        Next

        For b As Integer = 86 To 89
            circulos(b).Fill = Brushes.Orange
        Next

        For c As Integer = 75 To 77
            circulos(c).Fill = Brushes.Orange
        Next
        circulos(65).Fill = Brushes.Orange
        circulos(66).Fill = Brushes.Orange
        circulos(56).Fill = Brushes.Orange


        For c As Integer = 106 To 110
            circulos(c).Fill = Brushes.MediumPurple
        Next

        For b As Integer = 94 To 97
            circulos(b).Fill = Brushes.MediumPurple
        Next

        For a As Integer = 83 To 85
            circulos(a).Fill = Brushes.MediumPurple
        Next
        circulos(74).Fill = Brushes.MediumPurple
        circulos(73).Fill = Brushes.MediumPurple
        circulos(64).Fill = Brushes.Purple
        For a As Integer = 0 To 44
            For b As Integer = 0 To 120
                If (Canvas.GetTop(circulos(b)) = Canvas.GetTop(fichas3(a))) And (Canvas.GetLeft(circulos(b)) = Canvas.GetLeft(fichas3(a))) Then
                    circulos(b).Fill = Brushes.Red
                End If
            Next
        Next
    End Sub
    'este metodo le da el orden de los tuno al tablero de 3 jugadore
    Private Sub turnoColor3()

        Select Case turno3
            Case 1
                For b As Integer = 0 To 14
                    If fichaSelecionar.Equals(fichas3(b)) Then
                        turno3 = 2
                        turnoValidar = True
                        lblColor.Background = Brushes.Orange
                        lblJugador.Content = lblNaranja.Content
                    End If
                Next

            Case 2
                For b As Integer = 15 To 29
                    If fichaSelecionar.Equals(fichas3(b)) Then
                        turno3 = 3
                        lblColor.Background = Brushes.Purple
                        turnoValidar = True
                        lblJugador.Content = lblMorado.Content
                    End If
                Next
            Case 3
                For b As Integer = 30 To 44
                    If fichaSelecionar.Equals(fichas3(b)) Then
                        turno3 = 1
                        turnoValidar = True
                        lblColor.Background = Brushes.LightGreen
                        lblJugador.Content = lblVerde.Content
                    End If
                Next


        End Select
        If turno3 = 1 Then
            Contadorturnos3jugadores = Contadorturnos3jugadores + 1
            lblMarcadorTurnos.Content = (Contadorturnos3jugadores)
        End If


    End Sub
    'Este metodo verificara todos lo movimientos y salto permitidos de la ficha
    Private Sub saltar(sender As Object)
        For contador As Integer = 0 To 120
            Dim X As Integer = 5
            Dim Y As Integer = 5
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 40) Then
                X = 40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 20) Then
                X = 20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 20) Then
                X = -20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 40) Then
                X = -40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender)) Then
                X = 0
            End If

            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) + 40) Then
                Y = 40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) - 40) Then
                Y = -40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender)) Then
                Y = 0
            End If
            If Not X = 5 And Not Y = 5 Then
                If Not circulos(contador).Fill.Equals(Brushes.Red) Then
                Else
                    For a As Integer = 0 To 120
                        If Canvas.GetLeft(circulos(a)) = (Canvas.GetLeft(sender) + X * 2) And Canvas.GetTop(circulos(a)) = (Canvas.GetTop(sender) + Y * 2) Then
                            If Not circulos(a).Fill.Equals(Brushes.Red) And Not circulos(a).Fill.Equals(Brushes.Pink) Then
                                circulos(a).Fill = Brushes.Pink
                                saltar(circulos(a))
                            End If
                        End If
                    Next
                End If
            End If

        Next
    End Sub
    'con este metodo selecionamos el control de usario y le enseñamos los movimientos validos para 3 jugadore
    Private Sub Tablero3MouseLeftButtonDown(sender As Object, e As MouseEventArgs)
        fichaSelecionar = sender
        regresarcolores3() 'este codigo regresa  los colores
        For contador As Integer = 0 To 120
            Dim X As Integer = 5
            Dim Y As Integer = 5
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 40) Then
                X = 40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 20) Then
                X = 20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 20) Then
                X = -20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 40) Then
                X = -40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender)) Then
                X = 0
            End If

            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) + 40) Then
                Y = 40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) - 40) Then
                Y = -40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender)) Then
                Y = 0
            End If
            If Not X = 5 And Not Y = 5 Then
                If Not circulos(contador).Fill.Equals(Brushes.Red) Then
                    circulos(contador).Fill = Brushes.Pink
                Else
                    For a As Integer = 0 To 120
                        If Canvas.GetLeft(circulos(a)) = (Canvas.GetLeft(fichaSelecionar) + X * 2) And Canvas.GetTop(circulos(a)) = (Canvas.GetTop(fichaSelecionar) + Y * 2) Then
                            If Not circulos(a).Fill.Equals(Brushes.Red) Then
                                circulos(a).Fill = Brushes.Pink
                                saltar(circulos(a))
                            End If
                        End If
                    Next
                End If
            End If
        Next

    End Sub
    'con este metodo ponemos las restriccion de turno, movimiento no valido, y  ha selecionar el ellipse de posicion de 3 jugadores
    Private Sub Tableros3MouseLeftButtonDown(sender As Object, e As MouseEventArgs)
        If Not (IsNothing(fichaSelecionar)) Then
            turnoColor3()
            If turnoValidar = True Then
                If sender.fill.Equals(Brushes.Pink) Then

                    Canvas.SetLeft(fichaSelecionar, CDbl(Canvas.GetLeft(sender)))
                    Canvas.SetTop(fichaSelecionar, CDbl(Canvas.GetTop(sender)))
                    Ganar3Jugadores()
                    fichaSelecionar = Nothing
                    regresarcolores3()
                    turnoValidar = False
                Else
                    MsgBox("movimiento no valido", MsgBoxStyle.Critical)

                End If
            Else
                MsgBox("No es su Turno", MsgBoxStyle.Critical)
            End If
        Else
            MsgBox("seleccione una ficha", MsgBoxStyle.Critical)
        End If
    End Sub
    'Con este metodo verificamos si han ganado la fichas de solo un color 3 jugadores
    Private Sub Ganar3Jugadores()
        Dim contadorVerde As Integer
        Dim contadorNaranja As Integer
        Dim contadorMorado As Integer
        Dim marcador As Integer
        contadorVerde = 0
        contadorNaranja = 0
        contadorMorado = 0
        marcador = 0
        For a As Integer = 0 To 14
            For b As Integer = 0 To 14
                If ((Canvas.GetLeft(fichas3(b)))) = (Canvas.GetLeft(ellepseVerificarVerde(a))) And (Canvas.GetTop(fichas3(b)) = (Canvas.GetTop(ellepseVerificarVerde(a)))) Then
                    contadorVerde = contadorVerde + 1
                    If contadorVerde = 15 Then
                        regresarcolores3()
                        tablero.Children.Clear()
                        contadorForTablero = 0
                        TableroEllipse()
                        controlUsuario3Jugadores()
                        turno = 1
                        lblColor.Background = Brushes.LightGreen
                        MsgBox("GANO VERDE XD...!  Movimientos:  " & Contadorturnos3jugadores, MsgBoxStyle.Information)
                        Contadorturnos3jugadores = 0
                        turno3 = 1
                        lblMarcadorTurnos.Content = Contadorturnos3jugadores
                    End If
                End If
            Next
        Next

        For a As Integer = 0 To 14
            For b As Integer = 15 To 29
                If ((Canvas.GetLeft(fichas3(b)))) = (Canvas.GetLeft(ellepseVerificarNaranja(a))) And (Canvas.GetTop(fichas3(b)) = (Canvas.GetTop(ellepseVerificarNaranja(a)))) Then
                    contadorNaranja = contadorNaranja + 1
                    If contadorNaranja = 15 Then
                        regresarcolores3()
                        tablero.Children.Clear()
                        contadorForTablero = 0
                        TableroEllipse()
                        controlUsuario3Jugadores()
                        turno = 1
                        lblColor.Background = Brushes.LightGreen
                        MsgBox("GANO NARANJA XD...!  Movimientos:  " & Contadorturnos3jugadores, MsgBoxStyle.Information)
                        Contadorturnos3jugadores = 0
                        turno3 = 1
                        lblMarcadorTurnos.Content = Contadorturnos3jugadores
                    End If
                End If
            Next
        Next

        For a As Integer = 0 To 14
            For b As Integer = 30 To 44
                If ((Canvas.GetLeft(fichas3(b)))) = (Canvas.GetLeft(ellepseVerificarMorado(a))) And (Canvas.GetTop(fichas3(b)) = (Canvas.GetTop(ellepseVerificarMorado(a)))) Then
                    contadorMorado = contadorMorado + 1
                    If contadorMorado = 15 Then
                        regresarcolores3()
                        tablero.Children.Clear()
                        contadorForTablero = 0
                        TableroEllipse()
                        controlUsuario3Jugadores()
                        turno = 1
                        lblColor.Background = Brushes.LightGreen
                        MsgBox("GANO MORADO XD...!  Movimientos:  " & Contadorturnos3jugadores, MsgBoxStyle.Information)
                        Contadorturnos3jugadores = 0
                        turno3 = 1
                        lblMarcadorTurnos.Content = Contadorturnos3jugadores
                    End If
                End If
            Next
        Next

    End Sub

    ' METODOS PARA 6 JUGADORE
    ' este metodo ayuda a borrar el color de los movimientos permitidos para 6 jugadore
    Private Sub regresarcolores()
        For a As Integer = 0 To 120
            circulos(a).Fill = Brushes.Cornsilk
        Next
        For a As Integer = 111 To 120
            circulos(a).Fill = Brushes.Aquamarine
        Next
        For a As Integer = 0 To 9
            circulos(a).Fill = Brushes.GreenYellow
        Next
        For a As Integer = 111 To 120
            circulos(a).Fill = Brushes.CornflowerBlue
        Next
        For a As Integer = 10 To 13
            circulos(a).Fill = Brushes.Black
        Next

        For b As Integer = 23 To 25
            circulos(b).Fill = Brushes.Black
        Next
        For c As Integer = 35 To 36
            circulos(c).Fill = Brushes.Black
            circulos(46).Fill = Brushes.Black
        Next
        For a As Integer = 19 To 22
            circulos(a).Fill = Brushes.Salmon
        Next
        For b As Integer = 32 To 34
            circulos(b).Fill = Brushes.Salmon
        Next
        For c As Integer = 44 To 45
            circulos(c).Fill = Brushes.Salmon
            circulos(55).Fill = Brushes.Salmon
        Next

        For a As Integer = 98 To 101
            circulos(a).Fill = Brushes.DarkOrange
        Next
        For b As Integer = 86 To 88
            circulos(b).Fill = Brushes.DarkOrange
        Next
        For c As Integer = 75 To 76
            circulos(c).Fill = Brushes.DarkOrange
            circulos(65).Fill = Brushes.DarkOrange
        Next
        For c As Integer = 107 To 110
            circulos(c).Fill = Brushes.MediumPurple
            circulos(74).Fill = Brushes.MediumPurple
        Next
        For b As Integer = 95 To 97
            circulos(b).Fill = Brushes.MediumPurple
        Next
        For a As Integer = 84 To 85
            circulos(a).Fill = Brushes.MediumPurple
        Next
        For a As Integer = 0 To 59
            For b As Integer = 0 To 120
                If (Canvas.GetTop(circulos(b)) = Canvas.GetTop(fichas(a))) And (Canvas.GetLeft(circulos(b)) = Canvas.GetLeft(fichas(a))) Then
                    circulos(b).Fill = Brushes.Red
                End If
            Next
        Next
    End Sub
    'este metodo le da el orden de los tuno al tablero de 6 jugadore
    Private Sub turnoColor()
        Select Case turno
            Case 1
                For b As Integer = 0 To 9
                    If fichaSelecionar.Equals(fichas(b)) Then
                        turno = 2
                        turnoValidar = True
                        lblColor.Background = Brushes.Black
                        lblJugador.Content = lblNegro.Content
                    End If
                Next
            Case 2
                For b As Integer = 20 To 29
                    If fichaSelecionar.Equals(fichas(b)) Then
                        turno = 3
                        lblColor.Background = Brushes.Orange
                        turnoValidar = True
                        lblJugador.Content = lblNaranja.Content()
                    End If
                Next
            Case 3
                For b As Integer = 40 To 49
                    If fichaSelecionar.Equals(fichas(b)) Then
                        turno = 4
                        turnoValidar = True
                        lblColor.Background = Brushes.CornflowerBlue
                        lblJugador.Content = lblAzul.Content()
                    End If
                Next
            Case 4
                For b As Integer = 10 To 19
                    If fichaSelecionar.Equals(fichas(b)) Then
                        turno = 5
                        turnoValidar = True
                        lblColor.Background = Brushes.MediumPurple
                        lblJugador.Content = lblMorado.Content()
                    End If
                Next
            Case 5
                For b As Integer = 50 To 59
                    If fichaSelecionar.Equals(fichas(b)) Then
                        turno = 6
                        turnoValidar = True
                        lblColor.Background = Brushes.Red
                        lblJugador.Content = lblRojo.Content()
                    End If
                Next
            Case 6
                For b As Integer = 30 To 39
                    If fichaSelecionar.Equals(fichas(b)) Then
                        turno = 1
                        turnoValidar = True
                        lblColor.Background = Brushes.LightGreen
                        lblJugador.Content = lblVerde.Content()
                    End If
                Next
        End Select
        If turno = 1 Then
            Contadorturnos = Contadorturnos + 1
            lblMarcadorTurnos.Content = (Contadorturnos)
        End If
    End Sub
    'con este metodo selecionamos el control de usario y le enseñamos los movimientos validos para 6 jugadore
    Private Sub TableroMouseLeftButtonDown(sender As Object, e As MouseEventArgs)
        fichaSelecionar = sender
        regresarcolores()
        For contador As Integer = 0 To 120
            Dim X As Integer = 5
            Dim Y As Integer = 5
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 40) Then
                X = 40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) + 20) Then
                X = 20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 20) Then
                X = -20
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender) - 40) Then
                X = -40
            End If
            If (Canvas.GetLeft(circulos(contador)) = Canvas.GetLeft(sender)) Then
                X = 0
            End If

            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) + 40) Then
                Y = 40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender) - 40) Then
                Y = -40
            End If
            If (Canvas.GetTop(circulos(contador)) = Canvas.GetTop(sender)) Then
                Y = 0
            End If
            If Not X = 5 And Not Y = 5 Then
                If Not circulos(contador).Fill.Equals(Brushes.Red) Then
                    circulos(contador).Fill = Brushes.Pink
                Else
                    For a As Integer = 0 To 120
                        If Canvas.GetLeft(circulos(a)) = (Canvas.GetLeft(fichaSelecionar) + X * 2) And Canvas.GetTop(circulos(a)) = (Canvas.GetTop(fichaSelecionar) + Y * 2) Then
                            If Not circulos(a).Fill.Equals(Brushes.Red) Then
                                circulos(a).Fill = Brushes.Pink
                                saltar(circulos(a))
                                ' saltar((Canvas.GetLeft(fichaSelecionar) + X * 2), (Canvas.GetTop(fichaSelecionar) + Y * 2))
                            End If
                        End If
                    Next
                End If
            End If

        Next
    End Sub
    'con este metodo ponemos las restriccion de turno, movimiento no valido, y  ha selecionar el ellipse de posicion de 6 jugadores
    Private Sub TablerosMouseLeftButtonDown(sender As Object, e As MouseEventArgs)
        ellipseSeleccionar = sender

        If Not (IsNothing(fichaSelecionar)) Then
            turnoColor()
            If turnoValidar = True Then
                ' If sender.fill.Equals(Brushes.Pink) Then
                Canvas.SetLeft(fichaSelecionar, CDbl(Canvas.GetLeft(sender)))
                Canvas.SetTop(fichaSelecionar, CDbl(Canvas.GetTop(sender)))
                Ganar()
                fichaSelecionar = Nothing
                regresarcolores()
                turnoValidar = False
                'Else
                'MsgBox("movimiento no valido", MsgBoxStyle.Critical)
                ' End If
            Else
                MsgBox("No es su Turno", MsgBoxStyle.Critical)
            End If
        Else
            MsgBox("seleccione una ficha", MsgBoxStyle.Information)
        End If
    End Sub
    ' limpia el ingreso de los nombres de los jugadores
    Private Sub borrarNombres()
        txtAzul.Background = Brushes.Black
        txtVerde.Background = Brushes.Black
        txtNaranja.Background = Brushes.Black
        txtMorado.Background = Brushes.Black
        txtNegro.Background = Brushes.Black
        txtRojo.Background = Brushes.Black
        txtAzul.Text() = ("")
        txtVerde.Text() = ("")
        txtNaranja.Text() = ("")
        txtMorado.Text() = ("")
        txtNegro.Text() = ("")
        txtRojo.Text() = ("")
        btntAceptarNombres.Content = ("")
        btntAceptarNombres.Background = Brushes.Black
        btn3.IsEnabled = False
        btn6.IsEnabled = False
        Verde.Content = ("")
        Azul.Content = ("")
        Negro.Content = ("")
        Morado.Content = ("")
        Naranja.Content = ("")
        Rojo.Content = ("")
        introduccion.Content = ("")
        btntAceptarNombres.IsEnabled = False
    End Sub
    'Con este metodo verificamos si han ganado la fichas de solo un color 6 jugadores
    Private Sub Ganar()
        Dim contadorVerde As Integer
        Dim contadorAzul As Integer
        Dim contadorRojo As Integer
        Dim contadorNegro As Integer
        Dim contadorNaranja As Integer
        Dim contadorMorado As Integer
        Dim marcador As Integer
        contadorVerde = 0
        contadorAzul = 0
        contadorRojo = 0
        contadorNegro = 0
        contadorNaranja = 0
        contadorMorado = 0
        marcador = 0
        For b As Integer = 0 To 9
            For a As Integer = 111 To 120
                If ((Canvas.GetLeft(fichas(b)))) = (Canvas.GetLeft(circulos(a))) And (Canvas.GetTop(fichas(b)) = (Canvas.GetTop(circulos(a)))) Then
                    contadorVerde = contadorVerde + 1
                    If contadorVerde = 10 Then
                        regresarcolores()
                        tablero.Children.Clear()
                        contadorForTablero = 0
                        TableroEllipse()
                        controlUsuario6Jugadores()
                        turno = 1
                        lblColor.Background = Brushes.LightGreen
                        MsgBox("GANO VERDE XD...!  Movimientos: " & Contadorturnos, MsgBoxStyle.Information)
                        marcador = lblMarcadorVerde.Content()
                        lblMarcadorVerde.Content = marcador + 1
                        Contadorturnos = 0
                        turno = 1
                        lblMarcadorTurnos.Content = Contadorturnos
                    End If
                End If
            Next
        Next

        For b As Integer = 10 To 29
            For a As Integer = 0 To 9
                If ((Canvas.GetLeft(fichas(b)))) = (Canvas.GetLeft(circulos(a))) And (Canvas.GetTop(fichas(b)) = (Canvas.GetTop(circulos(a)))) Then
                    contadorAzul = contadorAzul + 1
                    If contadorAzul = 10 Then
                        regresarcolores()
                        tablero.Children.Clear()
                        contadorForTablero = 0
                        TableroEllipse()
                        controlUsuario6Jugadores()
                        turno = 1
                        lblColor.Background = Brushes.LightGreen
                        MsgBox("GANO AZUL XD...!   Movimientos: " & Contadorturnos, MsgBoxStyle.Information)
                        marcador = lblMarcadorAzul.Content()
                        lblMarcadorAzul.Content = marcador + 1
                        Contadorturnos = 0
                        turno = 1
                        lblMarcadorTurnos.Content = Contadorturnos
                    End If
                End If
            Next
        Next

        For b As Integer = 50 To 59
            For a As Integer = 0 To 9
                If ((Canvas.GetLeft(fichas(b)))) = (Canvas.GetLeft(ellepseNegro(a))) And (Canvas.GetTop(fichas(b)) = (Canvas.GetTop(ellepseNegro(a)))) Then
                    contadorMorado = contadorMorado + 1
                    If contadorMorado = 10 Then
                        regresarcolores()
                        tablero.Children.Clear()
                        contadorForTablero = 0
                        TableroEllipse()
                        controlUsuario6Jugadores()
                        turno = 1
                        lblColor.Background = Brushes.LightGreen
                        MsgBox("GANO MORADO XD...!  Movimientos: " & Contadorturnos, MsgBoxStyle.Information)
                        marcador = lblMarcadorMorado.Content()
                        lblMarcadorMorado.Content = marcador + 1
                        Contadorturnos = 0
                        turno = 1
                        lblMarcadorTurnos.Content = Contadorturnos
                    End If
                End If
            Next
        Next

        For b As Integer = 20 To 29
            For a As Integer = 0 To 9
                If ((Canvas.GetLeft(fichas(b)))) = (Canvas.GetLeft(ellepseMorado(a))) And (Canvas.GetTop(fichas(b)) = (Canvas.GetTop(ellepseMorado(a)))) Then
                    contadorNegro = contadorNegro + 1
                    If contadorNegro = 10 Then
                        regresarcolores()
                        tablero.Children.Clear()
                        contadorForTablero = 0
                        TableroEllipse()
                        controlUsuario6Jugadores()
                        turno = 1
                        lblColor.Background = Brushes.LightGreen
                        MsgBox("GANO Negro XD...!  Movimientos: " & Contadorturnos, MsgBoxStyle.Information)
                        marcador = lblMarcadorNegro.Content()
                        lblMarcadorNegro.Content = marcador + 1
                        Contadorturnos = 0
                        turno = 1
                        lblMarcadorTurnos.Content = Contadorturnos
                    End If
                End If
            Next
        Next
        For b As Integer = 40 To 49
            For a As Integer = 0 To 9
                If ((Canvas.GetLeft(fichas(b)))) = (Canvas.GetLeft(ellepseRojo(a))) And (Canvas.GetTop(fichas(b)) = (Canvas.GetTop(ellepseRojo(a)))) Then
                    contadorNaranja = contadorNaranja + 1
                    If contadorNaranja = 10 Then
                        regresarcolores()
                        tablero.Children.Clear()
                        contadorForTablero = 0
                        TableroEllipse()
                        controlUsuario6Jugadores()
                        turno = 1
                        lblColor.Background = Brushes.LightGreen
                        MsgBox("GANO ANARANJADO XD...!  Movimientos: " & Contadorturnos, MsgBoxStyle.Information)
                        marcador = lblMarcadorAnaranjado.Content()
                        lblMarcadorAnaranjado.Content = marcador + 1
                        Contadorturnos = 0
                        turno = 1
                        lblMarcadorTurnos.Content = Contadorturnos
                    End If
                End If
            Next
        Next
        For b As Integer = 30 To 39
            For a As Integer = 0 To 9
                If ((Canvas.GetLeft(fichas(b)))) = (Canvas.GetLeft(ellepseNaranaja(a))) And (Canvas.GetTop(fichas(b)) = (Canvas.GetTop(ellepseNaranaja(a)))) Then
                    contadorRojo = contadorRojo + 1
                    If contadorRojo = 10 Then
                        regresarcolores()
                        tablero.Children.Clear()
                        contadorForTablero = 0
                        TableroEllipse()
                        controlUsuario6Jugadores()
                        turno = 1
                        lblColor.Background = Brushes.LightGreen
                        MsgBox("GANO ROJO XD...! Movimientos: " & Contadorturnos, MsgBoxStyle.Information)
                        marcador = lblMarcadorRojo.Content()
                        lblMarcadorRojo.Content = marcador + 1
                        Contadorturnos = 0
                        turno = 1
                        lblMarcadorTurnos.Content = Contadorturnos
                    End If
                End If
            Next
        Next


    End Sub

    'METODOS DE INICIO
    'Este boton pasa al modo juego despues de que ya todos los jugadores han sido ingresados
    Private Sub btntAceptarNombres_Click(sender As Object, e As RoutedEventArgs) Handles btntAceptarNombres.Click
        If click6 = True Then
            lblJugador.Content = lblVerde.Content
            If txtAzul.Text() = ("") Or txtVerde.Text() = ("") Or txtNaranja.Text() = ("") Or txtRojo.Text() = ("") Or txtMorado.Text() = ("") Or txtNegro.Text() = ("") Then

                MsgBox("INGRESE LOS NOMBRES DE LOS JUGADORES")
            Else
                lblAzul.Content = ("AZUL :  " & txtAzul.Text())
                lblVerde.Content = ("VERDE :  " & txtVerde.Text())
                lblNaranja.Content = ("ANARANJADO :  " & txtNaranja.Text())
                lblMorado.Content = ("MORADO :  " & txtMorado.Text())
                lblRojo.Content = ("ROJO :  " & txtRojo.Text())
                lblNegro.Content = ("NEGRO :  " & txtNegro.Text())
                txtAzul.Background = Brushes.Black
                txtVerde.Background = Brushes.Black
                txtNaranja.Background = Brushes.Black
                txtMorado.Background = Brushes.Black
                txtNegro.Background = Brushes.Black
                txtRojo.Background = Brushes.Black
                txtAzul.IsEnabled = True
                txtVerde.IsEnabled = True
                txtNaranja.IsEnabled = True
                txtMorado.IsEnabled = True
                txtNegro.IsEnabled = True
                txtRojo.IsEnabled = True
                controlUsuario6Jugadores()
                btnReset.IsEnabled = True
                borrarNombres()
                lblMarcadorTurnos.Content = 1
                For a As Integer = 0 To 120
                    circulos(a).IsEnabled = True
                Next
                click6 = False
            End If
        End If

        If click3 = True Then
            lblJugador.Content = lblVerde.Content
            If txtVerde.Text() = ("") Or txtNaranja.Text() = ("") Or txtMorado.Text() = ("") Then

                MsgBox("INGRESE LOS NOMBRES DE LOS JUGADORES")
            Else
                lblAzul.Content = ("")
                lblVerde.Content = ("VERDE :  " & txtVerde.Text())
                lblNaranja.Content = ("ANARANJADO :  " & txtNaranja.Text())
                lblMorado.Content = ("MORADO :  " & txtMorado.Text())
                lblRojo.Content = ("")
                lblNegro.Content = ("")
                lblMarcadorRojo.Content = ("")
                lblMarcadorAzul.Content = ("")
                lblMarcadorNegro.Content = ("")
                controlUsuario3Jugadores()
                btnReset.IsEnabled = True
                borrarNombres()
                For a As Integer = 0 To 120
                    circulos(a).IsEnabled = True
                Next
                click3 = False
            End If
        End If

    End Sub
    ' Este boton indica que seleccionaron la opcion de tablero para 3 jugadores
    Private Sub btn3_Click(sender As Object, e As RoutedEventArgs) Handles btn3.Click

        btn6.IsEnabled = False
        btn3.IsEnabled = False
        btnReset.IsEnabled = False
        txtVerde.IsEnabled = True
        txtNaranja.IsEnabled = True
        txtMorado.IsEnabled = True
        txtVerde.Background = Brushes.White
        txtNaranja.Background = Brushes.White
        txtMorado.Background = Brushes.White

        btntAceptarNombres.IsEnabled = True
        btntAceptarNombres.Content = ("Aceptar")
        btntAceptarNombres.Background = Brushes.White

        Verde.Content = ("VERDE")
        Morado.Content = ("Morado")
        Naranja.Content = ("Naranja")

        introduccion.Content = ("Ingrese los nombres de los jugadores")
        btn3.IsEnabled = False
        btn6.IsEnabled = False
        lblMarcadorMorado.Content = 0
        lblMarcadorAnaranjado.Content = 0
        lblMarcadorVerde.Content = 0
        click3 = True
        lblMarcadorTurnos.Content = 1

    End Sub
     ' Este boton indica que seleccionaron la opcion de tablero para 6 jugadores
    Private Sub btn6_Click(sender As Object, e As RoutedEventArgs) Handles btn6.Click
        txtAzul.IsEnabled = True
        txtVerde.IsEnabled = True
        txtNaranja.IsEnabled = True
        txtMorado.IsEnabled = True
        txtNegro.IsEnabled = True
        txtRojo.IsEnabled = True
        txtAzul.Background = Brushes.White
        txtVerde.Background = Brushes.White
        txtNaranja.Background = Brushes.White
        txtMorado.Background = Brushes.White
        txtNegro.Background = Brushes.White
        txtRojo.Background = Brushes.White
        btntAceptarNombres.IsEnabled = True
        btntAceptarNombres.Content = ("Aceptar")
        btntAceptarNombres.Background = Brushes.White
        Verde.Content = ("Verde")
        Azul.Content = ("Azul")
        Negro.Content = ("Negro")
        Morado.Content = ("Morado")
        Naranja.Content = ("Naranja")
        Rojo.Content = ("Rojo")
        introduccion.Content = ("Ingrese los nombres de los jugadores")
        btn3.IsEnabled = False
        btn6.IsEnabled = False
        lblMarcadorRojo.Content = 0
        lblMarcadorMorado.Content = 0
        lblMarcadorNegro.Content = 0
        lblMarcadorAnaranjado.Content = 0
        lblMarcadorAzul.Content = 0
        lblMarcadorVerde.Content = 0
        click6 = True
    End Sub
    'esten boton reinicia el juego y vuelve a elegir tablero
    Private Sub btnreset_Click(sender As Object, e As RoutedEventArgs) Handles btnReset.Click
        tablero.Children.Clear()
        contadorForTablero = 0
        TableroEllipse()
        btn3.IsEnabled = True
        btn6.IsEnabled = True
        turno = 1
        turno3 = 1
        lblColor.Background = Brushes.LightGreen

        txtAzul.Text() = ("")
        txtVerde.Text() = ("")
        txtNaranja.Text() = ("")
        txtMorado.Text() = ("")
        txtNegro.Text() = ("")
        txtRojo.Text() = ("")

        lblAzul.Content = ("AZUL")
        lblVerde.Content = ("VERDE")
        lblNaranja.Content = ("ANARANJADO")
        lblMorado.Content = ("MORADO")
        lblRojo.Content = ("ROJO ")
        lblNegro.Content = ("NEGRO ")
        btnReset.IsEnabled = False

    End Sub
    'este es el metodo principal donde llama a todo lo principal para empezar
    Private Sub Tablero_Initialized(sender As Object, e As EventArgs) Handles tablero.Initialized
        TableroEllipse()
        For a As Integer = 0 To 120
            circulos(a).IsEnabled = False
        Next

    End Sub

End Class
